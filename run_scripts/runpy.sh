#!/bin/bash
for ifI in ${1};
do
echo $ifI
source ~/miniconda3/etc/profile.d/conda.sh
conda activate DDC10Ana
python runPMTstrip.py ${ifI}
done
wait
echo "Run Complete"
