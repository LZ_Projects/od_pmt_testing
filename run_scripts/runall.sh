#!/bin/bash
if [ $# -lt 1 ]
then
 echo "Missing arguments!!! To run"
 echo "bash PMT_bin_batch.sh [waveform channel] [dir] [last file #] <-i [startingindex]> <-b [baseline_samples]> <-T [trigger channel]> <--pmt> <--win> <--bfile>"
 exit 1
fi
dirname=$1
num=$3
init=0
bsam=-1
pth=-1
trig=-1
invert=-1
win=-1
usebase=-1
sp=-1
winup=0
winlow=0
it=-1
outname="PMT_Trigger.root"
nsamps=-1

source /cvmfs/lz.opensciencegrid.org/LzBuild/release-latest/setup.sh


for j in ${dirname}/*.bin ;
do
 cmdarr=(./bin2root -wd ${dirname} -i $(basename "${j}"))

 echo "${cmdarr[@]}"
 "${cmdarr[@]}"
 #echo "file ${j} complete"
done
wait
finame=$(basename "$dirname")
hadd -f "${finame}.root" ${dirname}/*.root
