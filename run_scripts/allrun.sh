#!/bin/bash

source ~/miniconda3/etc/profile.d/conda.sh
conda activate DDC10Ana
python runPMTstrip.py $1 &> /dev/null &

echo "Runs Complete"
