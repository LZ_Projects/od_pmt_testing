#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

import os
from setuptools import setup, find_packages


def get_version():
    _globals = {}
    with open(os.path.join("ddc10_analysis", "version.py")) as version_file:
        exec(version_file.read(), _globals)
    return _globals["__version__"]


def get_requirements():
    reqs = []
    a = open("requirements.txt", 'r')
    for line in a:
        reqs.append(line)
    print(reqs)
    return reqs


setup(
    author="Sam Eriksen and Luke Korley",
    author_email='eriksesr@gmail.com',
    description="DDC10 Analysis. . package for summarizing ROOT TTrees",
    entry_points={
        'console_scripts': [
            'ddc10_analysis=ddc10_analysis.__main__:main',
        ],
    },
    install_requires=get_requirements(),
    license="Apache Software License 2.0",
    include_package_data=True,
    keywords=['ROOT', 'DDC10', 'PMT', 'analysis'],
    name='ddc10_analysis',
    packages=find_packages(include=['ddc10_analysis*']),
    version=get_version(),
    zip_safe=True,
)
