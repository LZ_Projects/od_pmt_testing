.. include:: ../README.rst
   :end-before: Quick Start

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation

.. toctree::
   :maxdepth: 1
   :caption: Code reference
   :glob:

   api/*