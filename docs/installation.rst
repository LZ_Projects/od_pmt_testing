Code Overview
==============

This repository is split into two parts;

1. DDC10 output converter: :code:`bin2root`
2. PMT analysis code: :code:`ddc10_analysis`

The :code:`bin2root` converter takes the binary output of the DDC10 and converts it to ROOT.
This is c++ code and is located in the
`bin_2_root_converter <https://gitlab.com/LZ_Projects/od_pmt_testing/-/tree/master/bin_2_root_converter>`_ directory.
The API for this code is currently not included in the documentation.
This will eventually be combined into the ddc10_analysis module.
The code is fairly short, so does not take long to understand.


The :code:`ddc10_analysis` is a python module that can be installed via pip and used on the command line.
The code is located in the
`ddc10_analysis <https://gitlab.com/LZ_Projects/od_pmt_testing/-/tree/master/ddc10_analysis>`_ directory.
The API for this code is included within this documentation.

**Other things to note**

There are some bash scripts to call things, but these are getting slightly dated and should be replaced by a python
wrapper.



Installing
==========

If you need to more than just run the python analysis, then you need to clone the repo;

.. code-block:: sh

   git clone git@gitlab.com:LZ_Projects/od_pmt_testing.git
   cd od_pmt_testing


Bin2Root
~~~~~~~~
To use this requires :code:`ROOT`.
:code:`ROOT` is in the conda :code:`environment.yml` so if an environment is started from that
then you will be able to run the code.

.. code-block:: sh

    conda env create -f environment.yml
    conda activate ddc10_analysis
    cd bin_2_root_converter
    source compile.sh

     # Run
    ./bin2root --help


DDC10 Analysis
~~~~~~~~~~~~~~

You do not need to download the repository for this, however if you download it, you don't need to install it.

**To install**

.. code-block:: sh

   # Install
   python setup.py install
   # or
   pip install .

   # Use
   ddc10_analysis --help


**Run Uninstalled**

.. code-block:: sh

   python -m ddc10_analysis --help
