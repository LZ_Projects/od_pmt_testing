.. image:: ../docs/images/logo.png
   :target: http://www.pattiann.com/webcam/paint.html

OD PMT Testing
==============

Code base for testing OD PMTs are behaving themselves.

This repository contains code that can;

* Convert DDC10 output to a ROOT file
* Perform standard PMT analysis
* Perform analysis on each channel recorded in parallel
* Produce a standardised set of histograms

Quick Start
-----------
To run just the DDC10 analysis code without doing ROOT conversion you can :code:`pip install`;

.. code-block:: sh

    pip install git+https://gitlab.com/LZ_Projects/od_pmt_testing


Installation and Usage
----------------------
Visit the `documentation <https://lz_projects.gitlab.io/od_pmt_testing/>`_ for full details.

