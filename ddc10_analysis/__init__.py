__author__ = """Sam Eriksen and Luke Korley"""
__email__ = """eriksesr@gmail.com"""

from . import analysis_utilities
from . import channel_analysis
from . import triggering
from . import pdf_writer
from .version import __version__
