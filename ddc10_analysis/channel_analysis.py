import uproot as up
import numpy as np
import awkward as awk
from . import analysis_utilities
import physt
import time
from . import progress_bars
from . import pdf_writer
from physt.histogram1d import Histogram1D


def run_channel_analysis(channel_number, args, f_name, show_progress=True):
    """
    Run analysis over a channel

    .. todo::
        Fix file. Why pass the f_name and use args.file?

    :param channel_number:
    :type channel_number: int
    :param args: 
    :type args: argparser.ArgumentParser
    :param f_name: 
    :type f_name: str
    :param show_progress: if True, show progress bar
    :type show_progress: bool
    :return: 
    :rtype: 
    """
    promPeak = 0
    out_name = "{}_Chan{}.pdf".format(f_name, channel_number)
    output_name = args.outdir + '/' + out_name
    with up.open(args.file) as upf:
        wave = upf["hSum_Chan{}".format(channel_number)].numpy()
        promPeak = np.argmax(wave[0])
    with physt.config.config.enable_free_arithmetics():
        channel_histograms = {
            "SumWave": Histogram1D(wave[1], wave[0], name="Average WaveForm", axis_name="Samples [10ns]")}
    print("Prompt Peak At: sample {}".format(promPeak))
    print("Output will be: {}".format(out_name))
    channel_histograms.update(event_loop([args.file], channel_number, promPeak, entry_steps=500,
                                         show_progress=show_progress))
    pdf_writer.write_to_pdf(channel_histograms, output_name)
    return {"Chan{}".format(channel_number): channel_histograms}


def get_root_branch_list():
    """
    ROOT branches needed for analysis.

    .. todo::
        Make list global?

    :return: list of root branch names
    :rtype: list[bytes]
    """
    branches = [b'raw_waveforms',
                b'fBaselinerms_V',
                b'fBaseline_V',
                b'bIsGood',
                b'fPulseHeight_V',
                b'fPulseCharge_pC',
                b'fPulsePeakTime',
                b'fCalibratedTime',
                b'fPulseLength75',
                b'fPulseLength95'
                ]
    return branches


def create_histogram_dictionary():
    """
    Create a dictionary containing empty histograms.

    :return: dictionary of histograms
    :rtype: dict[str, Histogram1D or Histogram2D]
    """
    hist_dict = {'PromptQ': physt.h1(None, "fixed_width", bin_width=0.1, name="Prompt Pulses", axis_name="Area [pC]",
                                     adaptive=True),
                 'PromptWin': physt.h1(None, "fixed_width", bin_width=0.1, name="Prompt Window", axis_name="Area [pC]",
                                       adaptive=True),
                 'NPrompt20': physt.h1(None, "fixed_width", bin_width=1, name="# Window Pulses +/- 20 samples",
                                       axis_name="#", adaptive=True),
                 'NPrompt10': physt.h1(None, "fixed_width", bin_width=1, name="# Window Pulses +/- 10 samples",
                                       axis_name="#", adaptive=True),
                 'NPrompt5': physt.h1(None, "fixed_width", bin_width=1, name="# Window Pulses +/- 5 samples",
                                      axis_name="#",
                                      adaptive=True),
                 'BasevRMS': physt.h2(None, None, "fixed_width", bin_widths=(0.1, 0.1), name="Baseline Vs Baseline RMS",
                                      axis_names=("Baseline [mV]", "Baseline RMS [mV]"), adaptive=True),
                 'AllQ': physt.h1(None, "fixed_width", bin_width=0.1, name="All Pulses", axis_name="Area [pC]",
                                  adaptive=True),
                 'T': physt.h1(None, "fixed_width", bin_width=10, name="Pulse Time", axis_name="T [10ns]",
                               adaptive=True),
                 'NT': physt.h1(None, "fixed_width", bin_width=10, name="Pulse Time (Prompt Charge Normalized)",
                                axis_name="T [10ns]", adaptive=True),
                 'TvQ': physt.h2(None, None, "fixed_width", bin_widths=(10, 0.1), name="Pulse Time Vs Pulse Charge",
                                 axis_names=("T [10ns]", "Area [pC]"), adaptive=True),
                 'TvNQ': physt.h2(None, None, "fixed_width", bin_widths=(10, 0.1),
                                  name="Pulse Time Vs Pulse Charge (Prompt Charge Normalized)",
                                  axis_names=("T [10ns]", "Area Ratio"), adaptive=True),
                 'QvNQ': physt.h2(None, None, "fixed_width", bin_widths=(0.1, 0.1),
                                  name="Pulse Charge Vs Normalized Charge",
                                  axis_names=("Area [pC]", "Area Ratio"), adaptive=True),
                 'QvH': physt.h2(None, None, "fixed_width", bin_widths=(0.1, 0.1), name="Pulse Charge Vs Pulse Height",
                                 axis_names=("Area [pC]", "Pulse Height [mV]"), adaptive=True)
                 }

    return hist_dict


def event_loop(file_list, channel, PrompPeak, entry_steps=100, show_progress=True):
    """
    Loop over events in a file for a given channel and fill histograms

    .. todo::
        Fill PromptWin

    .. todo::
        Fix progress bar error that occurs if there is an error putout by uproot;
        RuntimeWarning: invalid value encountered in multiply [channel_analysis.py:177]
        RuntimeWarning: invalid value encountered in greater [jagged.py:1035]
    
    :param file_list: ROOT file to examine
    :type file_list: list[str]
    :param channel: channel number
    :type channel: int
    :param PrompPeak: Sample where the prompt peak is
    :type PrompPeak: int
    :param fig_dir: location to save figures
    :type fig_dir: str
    :param entry_steps: Where to enter the ROOT file
    :type entry_steps: int
    :param fig_name: pdf name
    :type fig_name: str
    :param show_progress:
    :type show_progress: bool
    :return: histograms of the data
    :rtype: dict[str, Histogram1D or Histogram2D]
    """
    nEvs = 0
    nChunk = 0
    n_files = len(file_list)
    print('Processing {0} files'.format(n_files))

    mplots = create_histogram_dictionary()
    mbranches = get_root_branch_list()

    # Assume just one file so use events
    if show_progress:
        start_time = time.time()
        n_events = up.open(file_list[0])['Chan1'].get('nSamples').numentries
        progress_bars.progress_update(0, n_events, start_time, del_prev=False)

    for zapevt in up.iterate(file_list, 'Chan{}'.format(channel), mbranches, entrysteps=entry_steps):

        nChunk += 1
        nEvs += len(zapevt[b'fBaseline_V'].flatten())

        for branch in mbranches:
            zapevt[branch] = awk.fromiter(zapevt[branch])

        AT = zapevt[b'fPulsePeakTime'] - PrompPeak
        PromptQ = zapevt[b'fPulseCharge_pC'][(AT < 5) * (AT > -5)].max()
        PromptWin = analysis_utilities.winQ(zapevt[b'raw_waveforms'].regular(), init=PrompPeak - 3, end=PrompPeak + 4)
        evMask = zapevt[b'bIsGood'] * (PromptQ > -5) * (PromptQ < 150)
        evMask = evMask * 1
        NQ = zapevt[b'fPulseCharge_pC'] / PromptQ

        NPrompt20 = (AT < 20) * (AT > -20) * 1
        NPrompt20 = NPrompt20.sum()
        NPrompt10 = (AT < 10) * (AT > -10) * 1
        NPrompt10 = NPrompt10.sum()
        NPrompt5 = (AT < 5) * (AT > -5) * 1
        NPrompt5 = NPrompt5.sum()

        pMask = evMask * PromptQ * (zapevt[b'fPulseLength95'] > 2) * (1 - np.isinf(NQ)) * (NQ < 50)
        pMask = (pMask > 0) * 1

        if pMask.sum().sum() > 0:
            mplots['PromptQ'].fill_n(PromptQ[evMask > 0])
            mplots['PromptWin'].fill_n(PromptWin[evMask>0])
            mplots['AllQ'].fill_n(zapevt[b'fPulseCharge_pC'][pMask > 0].flatten())
            mplots['T'].fill_n(AT[pMask > 0].flatten())
            mplots['QvNQ'].fill_n(
                np.array([zapevt[b'fPulseCharge_pC'][pMask > 0].flatten(), NQ[pMask > 0].flatten()]).T)
            mplots['QvH'].fill_n(np.array(
                [zapevt[b'fPulseCharge_pC'][pMask > 0].flatten(), zapevt[b'fPulseHeight_V'][pMask > 0].flatten() * 1e3],
                dtype=np.dtype('f8')).T)
            mplots['TvQ'].fill_n(np.array([AT[pMask > 0].flatten(), zapevt[b'fPulseCharge_pC'][pMask > 0].flatten()]).T)
            mplots['TvNQ'].fill_n(np.array([AT[pMask > 0].flatten(), NQ[pMask > 0].flatten()], dtype=np.dtype('f8')).T)
            mplots['BasevRMS'].fill_n(np.array([zapevt[b'fBaseline_V'][evMask > 0].flatten() * 1e3,
                                                zapevt[b'fBaselinerms_V'][evMask > 0].flatten() * 1e3],
                                               dtype=np.dtype('f8')).T)
            mplots['NT'].fill_n(AT[pMask > 0].flatten(), weights=1.0 / NQ[pMask > 0].flatten())
            mplots['NPrompt20'].fill_n(NPrompt20[evMask > 0])
            mplots['NPrompt10'].fill_n(NPrompt10[evMask > 0])
            mplots['NPrompt5'].fill_n(NPrompt5[evMask > 0])
        if show_progress:
            progress_bars.progress_update(nEvs, n_events, start_time, del_prev=True)
    if show_progress:
        progress_bars.progress_update(nEvs, n_events, start_time, del_prev=True)
        print('Finished || {} Events Processed'.format(nEvs))
    return mplots
