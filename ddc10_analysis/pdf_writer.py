from . import channel_analysis
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from physt.histogram1d import Histogram1D
from physt.histogram_nd import Histogram2D
import matplotlib as mpl
from pylab import rcParams

mpl.use('Agg')
rcParams['figure.figsize'] = 22, 11
rcParams['font.size'] = 24
rcParams["savefig.format"] = 'png'


def write_to_pdf(hist_dict, fig_name, log=False):
    """
    Write histograms in dictionary to pdf file.

    :param hist_dict: dictionary of histograms
    :type hist_dict: dict[str, Histogram1D or Histogram2D]
    :param fig_name: location and name to save pdf
    :type fig_name: str
    :param log: if True, give information about writing
    :type log: bool
    :return:
    :rtype: None
    """
    with PdfPages(fig_name, 'a') as pdf:
        for h in hist_dict.keys():
            fig, ax = plt.subplots()
            ndims = hist_dict[h].ndim
            nelem = hist_dict[h].total
            if log:
                print('Attempting to Write [{}] with {} elements'.format(hist_dict[h].name, nelem))
            if nelem == 0:
                continue
            elif ndims > 1:
                hist_dict[h].plot("image", ax=ax, cmap_normalize="log", cmap="rainbow", show_colorbar=True)
            else:
                hist_dict[h].plot(ax=ax, yscale="log", errors=True, show_stats=True)
            ax.set_title(hist_dict[h].name)
            pdf.savefig(fig)


def write_all_to_pdf(histograms, out_name, out_shape):
    """
    Write histograms in dictionary to pdf file.
    
    :param histograms:
    :type histograms: dict
    :param out_name: 
    :type out_name: str
    :param out_shape:
    :type out_shape: list[int, int]
    :return: 
    :rtype: None
    """
    with PdfPages(out_name) as pdf:
        hkeys = ["SumWave"] + list(channel_analysis.create_histogram_dictionary().keys())
        for hkey in hkeys:
            fig, ax = plt.subplots(out_shape[0], out_shape[1])
            for channel_i in range(len(histograms.keys())):
                achan = "Chan{}".format(channel_i + 1)
                iax = np.unravel_index(channel_i, out_shape)
                n_dims = histograms[achan][hkey].ndim
                nelem = histograms[achan][hkey].total
                print('Attempting to Write [{}] with {} elements'.format(histograms[achan][hkey].name, nelem))
                scale = "log"
                if "Wave" in hkey:
                    scale = ""
                if nelem == 0:
                    continue
                elif n_dims > 1:
                    histograms[achan][hkey].plot("image", ax=ax[iax], cmap_normalize="log", cmap="rainbow",
                                                 show_colorbar=True)
                else:
                    histograms[achan][hkey].plot(ax=ax[iax], yscale=scale, errors=True, show_stats=True)
                ax[iax].set_title(achan)
            fig.suptitle(histograms["Chan1"][hkey].name)
            pdf.savefig(fig)
