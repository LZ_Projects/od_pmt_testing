"""
Perform PMT analysis
"""

from . import channel_analysis
import functools
import sys
import os
import json
from multiprocessing import Pool
import matplotlib as mpl
from pylab import rcParams
from . import pdf_writer
from .version import __version__

mpl.use('Agg')
rcParams['figure.figsize'] = 22, 11
rcParams['font.size'] = 24
rcParams["savefig.format"] = 'png'


def create_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--file', type=str, help='Which file to run over: may include wildcards')
    parser.add_argument('-p', '--parallel', type=int, help="Number of processes to run in parallel", default=6)
    parser.add_argument('-c', '--channels', nargs='+', type=int, help="Range of Channels to run over", default=[1, 7])
    parser.add_argument('-s', '--shape', nargs='+', type=int,
                        help="Shape of output plots [must be compatible with channel range]", default=[2, 3])
    parser.add_argument('-o', '--outdir', type=str, help='Directory to save output pdfs to', default='./figs/')
    parser.add_argument('-v', '--version', action="version", version='%(prog)s ' + __version__)

    return parser


def main(args=None):
    """
    .. todo::
        Add tqdm progress bar for multiprocessing. process_map most likely

    :param args:
    :type args:
    :return:
    :rtype:
    """
    args = create_parser().parse_args(args)
    if len(sys.argv) == 1:
        create_parser().print_help()
        sys.exit(1)
    file_name = os.path.splitext(os.path.basename(args.file))[0]

    # Check if directory exists and if it doesn't make it
    if not os.path.exists(args.outdir):
        os.system("mkdir -p {0}".format(args.outdir))

    with open("{}_Hists.json".format(file_name), "w") as uf:
        histograms = {}
        channel_ids = list(range(*args.channels))
        if args.parallel == 1:
            single_core_progress_bar = True
        else:
            single_core_progress_bar = False
        with Pool(processes=args.parallel) as pool:
            result = pool.map(functools.partial(channel_analysis.run_channel_analysis, args=args, f_name=file_name,
                                                show_progress=single_core_progress_bar), channel_ids)
        histograms = {k: v for d in result for k, v in d.items()}
        output_name = args.outdir + "/{}_AllChans.pdf".format(file_name)
        pdf_writer.write_all_to_pdf(histograms, output_name, args.shape)
        for channel in histograms:
            for hke in histograms[channel]:
                histograms[channel][hke] = histograms[channel][hke].to_json()
        json.dump(histograms, uf)
    return 0


if __name__ == "__main__":
    main()
