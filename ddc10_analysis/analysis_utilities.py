import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
import awkward as awk
import collections
from scipy.optimize import curve_fit
from scipy.stats import norm, poisson, expon, chisquare
import matplotlib as mpl
from pylab import rcParams

rcParams['figure.figsize'] = 15, 11
rcParams['figure.figsize'] = 15, 11
mpl.rc('axes.formatter', useoffset=False)

ClockDDC10 = 6e8
adccperVolt = 8192
resistance_ohm = 50
sampleWidth_ns = 10


def winQ(wave, init=175, end=250, evMask=True):
    """
    Calculate the charge window - using simpson integral

    :param wave: The raw waveforms
    :type wave:
    :param init: Initial Sample
    :type init: int
    :param end: Final Sample
    :type end: int
    :param evMask: Not sure why it's needed?
    :type evMask: bool
    :return:
    :rtype:
    """
    sumax = len(wave.shape) - 1
    wmask = 1
    # print(wave.shape)
    wmask1 = np.indices(wave.shape)[1] > init
    wmask *= wmask1

    wmask1 = np.indices(wave.shape)[1] < end
    wmask *= wmask1
    qArr = 1e3 * integrate.simps(evMask * wmask * wave) * sampleWidth_ns / resistance_ohm

    return qArr


def peakHist(waveArr, chan=0, yrange=None, yscale=1, ret=False, doplot=True):
    peakT = np.argmax(waveArr[0][:, chan, :], axis=1)
    peakV = waveArr[0][np.arange(0, waveArr[1]['numEvents']), chan, peakT] * 1e3
    pHist = np.histogram2d(peakT, peakV, bins=[waveArr[1]['numSamples'], int(adccperVolt / yscale)])
    if doplot:
        plt.pcolormesh(pHist[1][:-1], pHist[2][:-1], np.transpose(pHist[0]) / waveArr[1]['totliveTime_s'],
                       norm=mpl.colors.LogNorm())
        cbar = plt.colorbar()
        plt.xlabel("peak Time (samples)")
        plt.ylabel("peak Amplitude (mV)")
        if isinstance(yrange, (tuple, list)):
            plt.ylim(yrange)
        plt.show()
        plt.plot(pHist[1][:-1], np.sum(pHist[0], axis=1))
        plt.xlabel("peak Time (samples)")
        plt.show()
        plt.plot(pHist[2][:-1], np.sum(pHist[0], axis=0))
        plt.xlabel("peak Amplitude (mV)")
        plt.show()
    if ret:
        return pHist, peakT, peakV
    else:
        return pHist


def plotWaves(waveArr, chan=0, nWaves=100):
    plt.figure()
    for i in range(min(nWaves, len(waveArr))):
        plt.plot(waveArr[i, chan, :], marker='+')
    plt.xlabel('samples (10ns)')
    plt.ylabel('V')
    plt.show()
    return plt.gcf()


def gpn(q, n, q0, q1, s0, s1, u, yq=0, ys=1, Ny=0):
    if n == 0:
        return norm.pdf(q, q0, np.abs(s0)) * float(poisson.pmf(0, u)) + Ny * expon.pdf(q, yq, ys)
    else:
        sn = s0 * s0 + (n * s1 * s1)
        gan = norm.pdf(q, q0 + n * q1, np.sqrt(sn)) * float(poisson.pmf(n, u))
        return gan + gpn(q, n - 1, q0, q1, s0, s1, u, yq, ys)


def fitQP(Qhist, P, N=50, doErr=False, dof=0):
    P = collections.OrderedDict(P)

    ng = len(P)
    mx = Qhist[1]
    mN = Qhist[0].sum() * (mx[1] - mx[0])
    my = Qhist[0] / mN
    merr = None
    abSig = None
    if doErr:
        args = Qhist[3]
        mx = mx[args]
        my = my[args]
        merr = np.sqrt(Qhist[2][args] / (mN * mN))
        abSig = True

    if 'yq' not in P and 'ys' not in P:
        lambdgpn = lambda q, q0, q1, s0, s1, u: gpn(q, N, q0, q1, s0, s1, u)
    elif 'yq' not in P:
        P['yq'] = 0
        lambdgpn = lambda q, q0, q1, s0, s1, u, yq, ys: gpn(q, N, q0, q1, s0, s1, u, yq, ys, 1)
    elif 'ys' not in P:
        P['ys'] = 1
        lambdgpn = lambda q, q0, q1, s0, s1, u, yq, ys: gpn(q, N, q0, q1, s0, s1, u, yq, ys, 1)

    fit, tmp = curve_fit(lambdgpn, mx, my, p0=list(P.values()), sigma=merr, absolute_sigma=abSig, maxfev=10000,
                         ftol=1e-8, gtol=1e-8)
    mchi2 = chisquare(my, gpn(mx, N, *fit), ddof=dof)
    # print(fit)
    params = P.copy()
    params.update(zip(params, fit))
    paramerr = params.copy()
    paramerr.update(zip(paramerr, np.diag(tmp)))
    params['chi2'] = mchi2
    params['norm'] = mN
    return params, paramerr


def LoG_kernel(sigma=1, scale=5, norm=False):
    """
    Create the kernel for Laplacian of Gaussian edge finding filter

    :param sigma:
    :type sigma:
    :param scale:
    :type scale:
    :param norm:
    :type norm:
    :return:
    :rtype:
    """
    sigma2 = sigma * sigma
    size = int(scale * sigma)
    x = (np.arange(size) - (size - 1) / 2.0)
    kernel = (x * x / sigma2 - 1) / sigma2
    N = 1 / (sigma * np.sqrt(2 * np.pi)) if norm else 1
    x2 = N * np.exp(-x * x / (2 * sigma2))
    print(x)
    print(kernel)
    LoG = kernel * x2
    return LoG


# filter performed using signal.fftconvolve() which allows convolution along axis, i.e. all filtered wave forms generated in one line utiizing scipy optimizations

# calculate minimum maximum and gradient of sliding window of Filtered Waveform
def mmg_rolling(a, window):
    axis = -1
    shape = a.shape[:axis] + (a.shape[axis] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    rolling = np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    grad = (rolling[..., -1] - rolling[..., 0]) / float(window - 1.0)
    return np.max(rolling, axis=axis), np.min(rolling, axis=axis), grad


# zero crossings of filtered waveform are candidate edges, gradient discriminates rising edge v falling edge candidates
# -1 elements are rising edge candidates and +1 elements are falling edge candidates
def zero_crossing(mLoG, mWave, thresh, window=3):
    maxL, minL, gradL = mmg_rolling(mLoG, window)
    # print(grad2L,gradL)
    pzero = (mLoG[..., 1:-1] > 0)
    zeroCross = np.zeros(shape=mLoG.shape).astype(np.int)
    zeroCross[..., 1:-1] = pzero * (minL < 0) + (1 - pzero) * (maxL > 0)
    diffL = maxL - minL
    zeroCross[..., 1:-1] = zeroCross[..., 1:-1] * (diffL > thresh)
    zeroCross[..., 1:-1] = ((gradL > thresh / window).astype(np.int) - (gradL < -thresh / window).astype(
        np.int)) * zeroCross[..., 1:-1]

    # zeroCross[zeroCross==0] = np.nan
    lEd = awk.fromiter([np.nonzero(zi)[0] for zi in zeroCross < 0])
    rEd = awk.fromiter([np.nonzero(zi)[0] for zi in zeroCross > 0])
    return (lEd, rEd), np.pad(gradL, [(0,)] * (gradL.ndim - 1) + [(1,)], 'constant', constant_values=(0))

# Now sort through candidate edges
# Maybe integrate from every left edge to every right edge (one sided search)
# Find minima between left right edge pairs
# Set a min threshold for the integral
# All edge combinations with integral above threshold kept
# Sort kept ranges by size
# Starting from smallest range integrate beyond boundary (both sides) until fractional change in integral is <1% (set as new left and right edge)
# Now look for overlapping pulses and merge
