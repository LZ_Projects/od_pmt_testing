import time
from IPython.display import clear_output
import sys


def progress_update(i, n_files, start_time, del_prev=True):
    """
    Print out progress status to screen

    Print progress bar, running time, remaining time and average processing time.
    Designed to be called within a loop on jupyter.
    Note; that if the update rate is too large, jupyter will stop outputting.

    :param i: for i in range(n_files)
    :type i: int
    :param n_files: number of files or events to process
    :type n_files: int
    :param start_time: processing start time (start_time = time.time() before for loop)
    :type start_time: float
    :param del_prev: Overwrite the previous progress bar. Needs to be true if on command line and not the first time.
    :type del_prev: bool
    :return: None

    :Example:

        .. code-block:: sh

            Number to process = 18
            Progress: [####################] 100.0%
            Average time per file 6.4s
            Time: 00:01:55
            Remaining time 00:00:00

    """
    progress = i / n_files
    bar_length = 20
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
    if progress < 0:
        progress = 0
    if progress >= 1:
        progress = 1
    block = int(round(bar_length * progress))

    real_time = time.time() - start_time
    if i > 0:
        average_time = real_time / i
    else:
        average_time = 0.0
    expected_time = (n_files - i) * average_time
    expected_time = time.strftime("%H:%M:%S", time.gmtime(expected_time))
    real_time = time.strftime("%H:%M:%S", time.gmtime(real_time))
    # For jupyter
    clear_output(wait=True)
    # For command line
    if del_prev:
        for _ in range(5):
            sys.stdout.write("\033[F\x1b[K")
    text = "Number to process = {0} \n" \
           "Progress: [{1}] {2:.1f}% \n" \
           "Average time {3:.1f}s \n" \
           "Time: {4} \n" \
           "Remaining time {5}".format(n_files, "#" * block + "-" * (bar_length - block), progress * 100,
                                       average_time, real_time, expected_time)
    print(text)
