########################################################
#
# Title: compile.sh
#
# Description: Compile the binary to root converter.
#
# Author: Sam Eriksen
#
########################################################

# Set some variables
ROOT_DIRECTORY=${HOME}/miniconda3/envs/ddc10_analysis/include/ROOT
OUT_NAME=bin2root
IN_NAME=bin2root.cc

# Enter the conda environment
conda activate ddc10_analysis

# Compile binary_to_root
g++ -o ${OUT_NAME} ${IN_NAME} -I ${ROOT_DIRECTORY} `root-config --cflags --libs`