// Standard includes
#include <cstdio>
#include <cstdlib>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <vector>

// ROOT includes
#include <TAxis.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TF1.h>
#include <TFile.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH2Poly.h>
#include <TKey.h>
#include <TLine.h>
#include <TMath.h>
#include <TMatrix.h>
#include <TNtuple.h>
#include <TObject.h>
#include <TProfile.h>
#include <TString.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TTree.h>
#include <TVector3.h>
#include <TVectorD.h>

using namespace std;
ifstream fin;
bool signal_start = false;
bool debug_mode = false;

bool lim_sams = false;

bool smoothing = false;
bool rolling = false;
bool triangle = false;
bool boxsmoothing = false;

bool isgood = true;

// define pulse finding parameters
double pulseThresh = 6.0;
double trigPulseThresh = 6.0;
double windowSize = 3.0;
double edgeThresh = 4.0;
double lookforward = 3.0;
int current_sweep = 0;
double number_of_peaks = 0.0;
int adc_per_Volt = 8192;
double resistance = 0.005;
int baseline_samples_set = 40;
int promptwindow = 300;
// smoothing parameters
int MovingWindowSize;
int iteration = 0.0;
int pth = 3.5;
int windowstart = 35;
int windowfin = 60;

// input parameters
int number_of_samples;
int Nchannels;
int Nevts;
short int* buff;

// internal run tracking
vector<float> raw_waveform;

vector<float> startv;
vector<float> endv;
vector<float> start_pointv;
vector<float> end_pointv;
vector<float> baseline;
vector<float> baselinev;
vector<float> trigbaselinev;
vector<float> pulse_left_edge;
vector<float> pulse_right_edge;

// output parameters
const int kMaxPulses = 2000;

float amplitude[kMaxPulses];
float charge_v[kMaxPulses];
float amplitude_position[kMaxPulses];
float chargeAmp[kMaxPulses];
float pl[kMaxPulses];
float pr[kMaxPulses];
float biggeststep[kMaxPulses];
// reconstruction params
float pulse_length99[kMaxPulses];
float pulse_length95[kMaxPulses];
float pulse_length90[kMaxPulses];
float pulse_length80[kMaxPulses];
float pulse_length75[kMaxPulses];
float pulse_length50[kMaxPulses];
float pulse_length25[kMaxPulses];
float pulse_length5[kMaxPulses];
float CalibratedTime[kMaxPulses];

float triggerHeight = 0;
float triggerPosition;
float triggerWidth;

float event_charge;
float event_charge_ten;
float event_baseline;
float event_rms;
float event_windowCharge;
int npulses = 0;
vector<double> vlivetime;

// Simpson Integral
double simpsIntegral(const std::vector<float> &samples, double baseline, int start, int end)
{
	int len;
	double qsum = 0.0;
	if ((end - start) % 2 == 0)
	{
		/* If there are an even number of samples, then there are an odd
		number of intervals; but Simpson's rule works only on an even
		number of intervals. Therefore we use Simpson's method on the
		all but the final sample, and integrate the last interval
		using the trapezoidal rule */
		len = end - start - 1;
		qsum += (samples[end - 1] + samples[end - 2] - 2 * baseline) / 2.0;
	}
	else
		len = end - start;

	double qsimps;
	qsimps = samples[start] - baseline;
	for (int i = start; i < start + len; i += 2)
		qsimps += (samples[i] - baseline) * 4;
	for (int i = start + 1; i < len + start - 1; i += 2)
		qsimps += (samples[i] - baseline) * 2;
	qsimps += samples[start + len - 1] - baseline;
	qsimps /= 3.0;

	qsum += qsimps;
	return qsum;
}

// Pulse Finding
void extract_event(vector<float>& v, double b, double rms, int nos, int trigger = 0, bool trig = false)
{

	double pThresh = (trig ? trigPulseThresh : pulseThresh) * rms * windowSize;
	double eThresh = edgeThresh * rms * windowSize;
	isgood = true;
	event_windowCharge = 0;
	if (trig)
		triggerHeight = 0;
	double temp_charge = 0;
	double temp_ten_charge = 0;
	double pulse_height_thresh = pulseThresh * rms;
	// cout<<" vector size is : "<<v.size()<<endl;
	// getchar();
	// Let's looking for the Pulses
	for (int i = 0; i < nos - windowSize; i++)
	{
		// std::cout << "Sample " << i << std::endl;
		double integral = simpsIntegral(v, b, i, i + windowSize);
		int left = 0;
		int right = 0;
		int temp_peak = 0;
		double temp_startv = 0;
		double temp_endv = 0;

		double temp_bigstep = 0;

		if ((v[i] - b) > pulse_height_thresh)
		{
			if (debug_mode)
			{
				cout << " This is sample : " << i << "; integral value is : " << integral
				     << "; pThresh is : " << pThresh << endl;
			}
			left = i;
			integral = 1.0e9;
			while ((integral) > eThresh && left > windowSize)
			{
				left--;
				integral = simpsIntegral(v, b, left, left + windowSize);
				temp_startv = v[left];
				if (debug_mode)
				{

					cout << "Left is  : " << left << " integral is : " << integral << "; eThresh is : " << eThresh
					     << endl;
					getchar();
				}
			}

			if (debug_mode)
				cout << "Starting right edge search" << endl;
			integral = 1.0e9;
			right = i + windowSize;
			double thischarge = 0;
			bool end = false;
			while (!end)
			{
				while ((integral) > eThresh && right < nos - 1)
				{
					right++;
					integral = simpsIntegral(v, b, right - windowSize, right);
					temp_endv = v[right];
					if (debug_mode)
					{

						cout << "right window is  : " << right << "; integral is : " << integral
						     << "; eThresh is : " << eThresh << endl;
						getchar();
					}
				}

				end = true;
				int r = right;
				while (r < fmin(fmin((int)v.size() - 1, nos - 1), right + lookforward))
				{
					r++;
					integral = simpsIntegral(v, b, r - windowSize, r);
					if ((integral) > pThresh)
					{
						right = r;
						end = false;
						if (debug_mode)
						{

							cout << "right lookforward is  : " << right << "; integral is : " << integral
							     << "; eThresh is : " << pThresh << endl;
							getchar();
						}
						break;
					}
				}
			}
			double max = -1.0e9;
			double totalq = simpsIntegral(v, b, left, right);
			double tempq5 = 0, tempq25 = 0, tempq50 = 0, tempq75 = 0, tempq90 = 0, tempq95 = 0, tempq99 = 0;
			for (int j = left; j < right; j++)
			{
				double s = v[j] - b;
				if ((s) > max)
				{
					max = s;
					temp_peak = j;
					if (j > 0 && (v[j] - v[j - 1]) > temp_bigstep)
						temp_bigstep = v[j] - v[j - 1];
				}
				double ratio = simpsIntegral(v, b, left, j) / totalq;
				if (ratio <= 0.05 && tempq5 < (j - left))
					tempq5 = j - left;
				if (ratio <= 0.25 && tempq25 < (j - left))
					tempq25 = j - left;
				if (ratio <= 0.5 && tempq50 < (j - left))
					tempq50 = j - left;
				if (ratio <= 0.75 && tempq75 < (j - left))
					tempq75 = j - left;
				if (ratio <= 0.9 && tempq90 < (j - left))
					tempq90 = j - left;
				if (ratio <= 0.95 && tempq95 < (j - left))
					tempq95 = j - left;
				if (ratio <= 0.99 && tempq99 < (j - left))
					tempq99 = j - left;
			}
			if (right > nos)
				continue;

			if (simpsIntegral(v, b, left, right) <= eThresh)
			{
				i = right - 1;
				continue;
			}
			i = right;

			startv.push_back(temp_startv);
			endv.push_back(temp_endv);
			if (signal_start)
			{
				npulses++;
				amplitude[npulses - 1] = max;
				amplitude_position[npulses - 1] = temp_peak;
				pl[npulses - 1] = left;
				pr[npulses - 1] = right;
				charge_v[npulses - 1] = simpsIntegral(v, b, left, right) / resistance;

				CalibratedTime[npulses - 1] = temp_peak - trigger;
				biggeststep[npulses - 1] = temp_bigstep;
				pulse_length5[npulses - 1] = tempq5;
				pulse_length25[npulses - 1] = tempq25;
				pulse_length50[npulses - 1] = tempq50;
				pulse_length75[npulses - 1] = tempq75;
				pulse_length90[npulses - 1] = tempq90;
				pulse_length95[npulses - 1] = tempq95;
				pulse_length99[npulses - 1] = tempq99;
				chargeAmp[npulses - 1] = charge_v[npulses - 1]/max;
				if (left < baseline_samples_set)
					isgood = false;
				temp_charge += charge_v[npulses - 1];
				if (i < promptwindow)
					temp_ten_charge += charge_v[npulses - 1];
				pulse_left_edge.push_back(left);
				pulse_right_edge.push_back(right);
			}
			else if (triggerHeight < max)
			{
				triggerHeight = max;
				triggerPosition = left;
				triggerWidth = right - left;
				pulse_left_edge.push_back(left);
				pulse_right_edge.push_back(right);
			}

		} // if statement
	}
	if (!trig)
	{
		event_charge_ten = temp_ten_charge;
		event_charge = temp_charge;
		event_windowCharge = simpsIntegral(v, b, trigger + windowstart, trigger + windowfin) / resistance;
	}
}
// Find the baseline
double baseline_rms(vector<float>& v, vector<float>& sample, double* irms)
{
	double rms = 0;
	double temp_base = 0;
	double baseline_samples = 0;
	for (int k = 0; k < v.size(); k++)
	{
		baseline_samples += v[k];
	}
	baseline_samples /= v.size();
	for (int i = 0; i < v.size(); i++)
	{
		rms += pow(v[i] - baseline_samples, 2);
	}
	rms = sqrt(rms / v.size());
	if (signal_start && debug_mode)
	{
		cout << " rms is : " << rms << " ;baseline is : " << baseline_samples << endl;
		getchar();
	}

	//}
	irms[0] = rms;
	return baseline_samples;
}

// Trigger analysis
double Trigger_info(vector<float> waveform)
{
	double rms_trigger = 0;
	double base_trigger = baseline_rms(trigbaselinev, waveform,
	    &rms_trigger); // Calculate baseline and rms then pass to pulse finder
	extract_event(waveform, base_trigger, rms_trigger, number_of_samples, 0, true);
	double time;
	baselinev.clear();
	if (pulse_left_edge.size() == 0)
	{
		time = 82;
		cout << " Can not find the trigger pulse for this event ! " << endl;
	}
	else
	{
		time = (pulse_left_edge.back());
	}
	pulse_left_edge.clear();
	pulse_right_edge.clear();
	trigbaselinev.clear();
	return time;
}

void getwaveform(vector<float>& v, int channel, int numread, float mult = 1, bool trig = false)
{
	int starti = ((current_sweep - numread) * Nchannels + channel) * (4 + 2 + number_of_samples) + 4;
	double datum;
	for (int i = 0; i < number_of_samples; i++)
	{
		datum = (double)buff[i + starti] * mult / (double)adc_per_Volt;
		v.push_back(datum);
		if (i < baseline_samples_set)
		{
			if (!trig)
				baselinev.push_back(datum);
			else
				trigbaselinev.push_back(datum);
		}
	}
}
int calcnumchannels(int mask)
{
	int numchans = 0;
	while (mask > 0)
	{
		numchans += mask % 2;
		mask /= 2;
	}
	return numchans;
}

bool readlivetime(char datafilename[])
{

	char linea[250];
	cout << "Attempting to read log file" << endl;
	TString filenameform = datafilename;
	filenameform.ReplaceAll(".bin", ".log");
	ifstream loginfile;
	loginfile.open(filenameform.Data(), ios::in);
	if (!loginfile.is_open())
		return false;
	else
	{
		// first five dummy lines
		loginfile.getline(linea, 250);
		loginfile.getline(linea, 250);
		loginfile.getline(linea, 250);
		loginfile.getline(linea, 250);
		loginfile.getline(linea, 250);
	}

	vlivetime.resize(Nevts);
	int evtcounter = 0;
	while (loginfile.getline(linea, 250) && evtcounter < Nevts)
	{
		evtcounter++;
		std::stringstream blank(linea);
		int iev = -1, dum1 = -1;
		long int liveentry = -1;
		char c = ',';
		std::string item;
		if (std::getline(blank, item, c))
			iev = std::atoi(item.data());
		if (std::getline(blank, item, c))
			dum1 = std::atoi(item.data());
		if (std::getline(blank, item, c))
			liveentry = std::atol(item.data());

		vlivetime[Nevts - evtcounter] = (double)liveentry / 6e8;
		if (liveentry == -1 || dum1 == -1 || iev == -1)
		{
			std::cout << "Reading logfile failed skipping" << std::endl;
			vlivetime.clear();
			return false;
		}
	}
	loginfile.close();
	return true;
}

static void show_usage(string name)
{
	cout << " Usage : ./DDC10_data_readout [-co] file1 " << name << " Options:\n"
	     << " -o : Name of output file.\n"
	     << " -i : Name of input file.\n"
	     << " -wd : Working directory\n"
	     << " -wform : Waveform channel\n"
	     << " -t : trigger channel\n"
	     << " -invert: invert waveform\n"
	     << " -trigger : invert trigger pulse \n"
	     << " -pt : pulse threshold\n"
	     << " -tri : Traiangle smoothing enabled\n"
	     << " -debug : Get in the debugging mode.\n"
	     << " -sit : number of smoothing interation\n"
	     << " -h or --help : Show the usage\n"
	     << " Enjoy ! -Ryan Wang" << endl;
}
int main(int argc, char* argv[])
{
	string filename;
	string outfilename;
	string working_dir;

	bool trigger_inversion = false;
	bool invert_waveform = false;

	int num_sams = 0;

	ifstream logfile;

	if (argc < 2)
	{
		show_usage(argv[0]);
		return 1;
	}
	for (int i = 1; i < argc; ++i)
	{
		string arg = argv[i];
		if ((arg == "-h") || (arg == "--help"))
		{
			show_usage(argv[0]);
			return 0;
		}
		else if (arg == "-wd")
		{
			working_dir = argv[i + 1];
		}
		else if (arg == "-i")
		{
			filename = argv[i + 1];
		}
		else if (arg == "-bs")
		{
			baseline_samples_set = atoi(argv[i + 1]);
		}
		else if (arg == "-pt")
		{
			pulseThresh = atof(argv[i + 1]);
		}
		else if (arg == "-win")
		{
			windowSize = atof(argv[i + 1]);
		}
		else if (arg == "-invert")
		{
			invert_waveform = true;
		}
		else if (arg == "-sams")
		{
			lim_sams = true;
			num_sams = atof(argv[i + 1]);
		}
		else if (arg == "-debug")
		{
			debug_mode = true;
		}
		else if (arg == "-spe")
		{
			windowstart = atoi(argv[i + 1]);
			windowfin = atoi(argv[i + 2]);
		}
	}

	double fixedbase = 0;
	double fixedrms = 0;

	short int datum;
	int dummy;
	int mask, size;

	char open_filename[200]; // full input filename
	sprintf(open_filename, "%s/%s", working_dir.c_str(), filename.c_str());
	cout << " We are opening " << open_filename << endl;
	fin.open(open_filename, ios::binary | ios::in | ios::ate);

	if (fin.is_open())
	{
		size = fin.tellg();
		fin.seekg(0, ios::beg);
		fin.read((char*)&Nevts, sizeof(Nevts));
		cout << Nevts << " Events" << endl;

		fin.read((char*)&number_of_samples, sizeof(number_of_samples));
		cout << number_of_samples << " Samples" << endl;

		fin.read((char*)&mask, sizeof(mask));
		cout << "Mask: " << mask << endl;
		Nchannels = calcnumchannels(mask);
		cout << Nchannels << " channels" << endl;

		fin.read((char*)&dummy, sizeof(dummy));
	}
	else
	{
		cout << "Failed to open file" << endl;
		return -1;
	}
	int predsize = Nevts * Nchannels * (2 * 4 + 2 * number_of_samples + 4) + 4 * 4;
	if (size < predsize)
	{
		cout << "Warning::Size predicted from header is greater than actual size" << endl;
		return -1;
	}

	int evtsize = Nchannels * (2 * 4 + 2 * number_of_samples + 4);
	int buffsize;
	if ((Nevts * evtsize) > 335544320)
		buffsize = 335544320 / evtsize;
	else
		buffsize = Nevts;
	cout << "Using buffer of " << buffsize << " events" << endl;

	readlivetime(open_filename);

	char out_filename[200]; // full output filename
	int iana = 0; // TGraph counter
	int pcount = 0; // Pulse counter
	double temp_sum = 0;
	double rms_value;

	TString ofilenameform = open_filename;
	ofilenameform.ReplaceAll(".bin", ".root");
	cout << " Out put filename is : " << ofilenameform << endl;
	TFile* fout = new TFile(ofilenameform.Data(), "RECREATE");

	float waveforms[8192];
	float trigger_t;
	double livetime;
	vector<TTree*> event;
	vector<TH1D*> h_sum;

	for (int chan = 1; chan < Nchannels; chan++)
	{
		char treename[20];
		sprintf(treename, "Chan%d", chan);
		string ttname = treename;
		h_sum.push_back(new TH1D(("hSum_" + ttname).c_str(), ("#font[132]{WFD " + ttname + " SumWaveForm}").c_str(),
		    number_of_samples, 0, number_of_samples));
		h_sum[chan - 1]->SetXTitle("#font[132]{Sample (2ns)}");
		h_sum[chan - 1]->GetXaxis()->SetLabelFont(132);
		h_sum[chan - 1]->GetYaxis()->SetLabelFont(132);
		event.push_back(new TTree(treename, treename));
		// event->Branch("npulses",&npulses,"npulses/I");
		event[chan - 1]->Branch("nSamples", &number_of_samples, "number_of_samples/I");
		event[chan - 1]->Branch("raw_waveforms", waveforms, "waveforms[number_of_samples]/F");

		event[chan - 1]->Branch("dLiveTime_s", &livetime, "livetime/D");

		event[chan - 1]->Branch("fCharge_pC", &event_charge, "event_charge/F");
		event[chan - 1]->Branch("fBaseline_V", &event_baseline, "event_baseline/F");
		event[chan - 1]->Branch("fBaselinerms_V", &event_rms, "event_rms/F");
		event[chan - 1]->Branch("bIsGood", &isgood, "isgood/O");
		event[chan - 1]->Branch("nPulses", &npulses, "npulses/I");
		event[chan - 1]->Branch("fPulseHeight_V", amplitude, "amplitude[npulses]/F");
		event[chan - 1]->Branch("fPulseRightEdge", pr, "pr[npulses]/F");
		event[chan - 1]->Branch("fPulseLeftEdge", pl, "pl[npulses]/F");
		event[chan - 1]->Branch("fPulseCharge_pC", charge_v, "charge_v[npulses]/F");
		event[chan - 1]->Branch("fPulseChargeAmp_pSOhm", chargeAmp, "chargeAmp[npulses]/F");
		event[chan - 1]->Branch("fPulsePeakTime", amplitude_position, "amplitude_position[npulses]/F");
		event[chan - 1]->Branch("fCalibratedTime", CalibratedTime, "CalibratedTime[npulses]/F");
		event[chan - 1]->Branch("fPulseLength5", pulse_length5, "pulse_length5[npulses]/F");
		event[chan - 1]->Branch("fPulseLength25", pulse_length25, "pulse_length25[npulses]/F");
		event[chan - 1]->Branch("fPulseLength50", pulse_length50, "pulse_length50[npulses]/F");
		event[chan - 1]->Branch("fPulseLength75", pulse_length75, "pulse_length75[npulses]/F");
		event[chan - 1]->Branch("fPulseLength90", pulse_length90, "pulse_length90[npulses]/F");
		event[chan - 1]->Branch("fPulseLength95", pulse_length95, "pulse_length95[npulses]/F");
		event[chan - 1]->Branch("fPulseLength99", pulse_length99, "pulse_length99[npulses]/F");

		event[chan - 1]->Branch("fTriggerTime", &trigger_t, "trigger_t/F");
		event[chan - 1]->Branch("fTriggerHeight_V", &triggerHeight, "triggerHeight/F");
		event[chan - 1]->Branch("fTriggerWidth", &triggerWidth, "triggerWidth/F");
		event[chan - 1]->Branch("fWindowCharge_pC", &event_windowCharge, "event_windowCharge/F");
	}
	// Store the waveform plot for debugging
	TCanvas* waveplot;
	vector<float> baseline_sweep;
	vector<float> trigwaveform;

	int skip = 0;
	int readin = 0;
	int lastadd = 0;
	for (int sweep = 0; sweep < Nevts; sweep++)
	{
		while ((readin) < (sweep + 1))
		{
			int arrsize = buffsize * evtsize / 2;
			if ((Nevts - readin) < buffsize)
			{
				arrsize = (Nevts - readin) * evtsize / 2;
			}
			buff = new short int[arrsize];
			fin.read((char*)&buff[0], arrsize * sizeof(buff[0]));
			lastadd = readin;
			readin += arrsize * 2 / evtsize;

			cout << "Read in " << readin << " events so far" << endl;
		}

		current_sweep = sweep;

		signal_start = false;
		getwaveform(trigwaveform, 0, lastadd, (trigger_inversion ? -1.0 : 1.0), true);
		trigger_t = Trigger_info(trigwaveform);
		trigwaveform.clear();

		livetime = vlivetime[Nevts - sweep - 1];
		vlivetime.pop_back();

		if (sweep % 1000 == 0)
		{
			cout << " This is sweep : " << sweep << endl;
			cout << "Trigger time: " << trigger_t << endl;
		}
		for (int wform_channel = 1; wform_channel < Nchannels; wform_channel++)
		{
			signal_start = true;
			getwaveform(raw_waveform, wform_channel, lastadd, (invert_waveform ? -1.0 : 1.0));

			std::copy(raw_waveform.begin(), raw_waveform.end(), waveforms);
			npulses = 0.0;
			double thisbase;
			rms_value = 0;

			thisbase = baseline_rms(baselinev, raw_waveform, &rms_value);
			for (int sam = 0; sam < number_of_samples; sam++)
			{
				h_sum[wform_channel - 1]->Fill(sam, raw_waveform[sam] - thisbase);
			}
			extract_event(raw_waveform, thisbase, rms_value, (lim_sams ? num_sams : number_of_samples), trigger_t);

			if (debug_mode)
			{
				cout << " basline is  : " << thisbase << " rms is : " << rms_value << endl;
				getchar();
			}
			event_baseline = thisbase;
			event_rms = rms_value;
			event[wform_channel - 1]->Fill();
			baselinev.clear();
			pulse_left_edge.clear();
			pulse_right_edge.clear();
			startv.clear();
			endv.clear();
			raw_waveform.clear();
		}
	}

	cout << " Total sweeps is : " << Nevts << endl;
	for (int k = 0; k < event.size(); k++)
	{
		event[k]->Write();
		h_sum[k]->Scale(1.0/(float)Nevts);
		h_sum[k]->Write();
	}
	fout->Write();
	fout->Close();

	return 0;
}
